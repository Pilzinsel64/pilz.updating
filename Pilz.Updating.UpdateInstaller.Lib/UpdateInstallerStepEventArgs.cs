﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.UpdateInstaller.Lib
{
    public class UpdateInstallerStepEventArgs : UpdateInstallerEventArgs
    {
        public UpdateInstallerStep Step { get; init; }
        public UpdateInstallerStepState State { get; init; }

        public UpdateInstallerStepEventArgs(UpdateInstaller updateInstaller, UpdateInstallerStep step, UpdateInstallerStepState state) : base(updateInstaller)
        {
            Step = step;
            State = state;
        }
    }
}
