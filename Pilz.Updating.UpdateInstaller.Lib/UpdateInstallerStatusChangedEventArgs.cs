﻿using System;

namespace Pilz.Updating.UpdateInstaller.Lib
{
    public class UpdateInstallerStatusChangedEventArgs : EventArgs
    {
        public UpdateInstallerStatus NewStatus { get; private set; }

        public UpdateInstallerStatusChangedEventArgs(UpdateInstallerStatus newStatus) : base()
        {
            NewStatus = newStatus;
        }
    }
}