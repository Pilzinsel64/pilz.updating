﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.UpdateInstaller.Lib
{
    public class UpdateInstallerEventArgs : EventArgs
    {
        public UpdateInstaller UpdateInstaller { get; init; }

        public UpdateInstallerEventArgs(UpdateInstaller updateInstaller)
        {
            UpdateInstaller = updateInstaller;
        }
    }
}
