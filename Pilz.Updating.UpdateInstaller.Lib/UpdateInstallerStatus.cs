﻿
namespace Pilz.Updating.UpdateInstaller.Lib
{
    public enum UpdateInstallerStatus
    {
        Waiting,
        Extracting,
        CopyingFiles,
        RemovingFiles,
        Done
    }
}