﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.UpdateInstaller.Lib
{
    public enum UpdateInstallerStepState
    {
        Default,
        PreEvent,
        PostEvent
    }
}
