﻿namespace Pilz.Updating
{
    public enum UpdateStatus
    {
        Waiting,
        Searching,
        DownloadingPackage,
        DownloadingInstaller,
        StartingInstaller
    }
}