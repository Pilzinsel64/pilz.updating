using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Pilz.Updating.Administration.Discord;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Administration.GUI
{
    public partial class DiscordBotSettingsWindow : RadForm
    {
        private readonly DiscordBotConfig config = null;
        private Channels currentChannel = Channels.Stable;
        private readonly bool hasInit = false;

        public DiscordBotSettingsWindow(DiscordBotConfig config)
        {
            this.config = config;

            InitializeComponent();

            foreach (var value in Enum.GetValues<Channels>())
                radDropDownList_PresetChannel.Items.Add(new RadListDataItem(Enum.GetName(value), value));
            radDropDownList_PresetChannel.SelectedValue = Channels.Stable;

            radTextBoxControl_BotToken.Text = config.DiscordBotToken;
            radTextBoxControl_DefaultProgramName.Text = config.DefaultAppName;
            ShowDefaultMessage();
            radToggleSwitch_UseProxy.Value = config.UseProxy;

            hasInit = true;
        }

        private Channels GetCurrentChannel()
        {
            return radDropDownList_PresetChannel?.SelectedValue as Channels? ?? Channels.Stable;
        }

        private void ShowDefaultMessage()
        {
            radTextBox_DefaultMessage.Text = config.DefaultUpdateMessages[currentChannel];
        }

        private void SaveDefaultMessage()
        {
            config.DefaultUpdateMessages[currentChannel] = radTextBox_DefaultMessage.Text.Trim();
        }

        private void ButtonX_Okay_Click(object sender, EventArgs e)
        {
            SaveDefaultMessage();
            config.DefaultAppName = radTextBoxControl_DefaultProgramName.Text.Trim();
            config.DiscordBotToken = radTextBoxControl_BotToken.Text.Trim();
            config.UseProxy = radToggleSwitch_UseProxy.Value;
        }

        private void radDropDownList_PresetChannel_SelectedValueChanged(object sender, EventArgs e)
        {
            if (hasInit)
                SaveDefaultMessage();

            currentChannel = radDropDownList_PresetChannel?.SelectedValue as Channels? ?? Channels.Stable;

            ShowDefaultMessage();
        }
    }
}