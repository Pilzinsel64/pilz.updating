﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace Pilz.Updating.Administration.GUI
{
    [DesignerGenerated()]
    public partial class UpdateManagerWindow : Telerik.WinControls.UI.RadRibbonForm
    {

        // Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Wird vom Windows Form-Designer benötigt.
        private System.ComponentModel.IContainer components;

        // Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
        // Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
        // Das Bearbeiten mit dem Code-Editor ist nicht möglich.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn1 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "Name");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn2 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "Version");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn3 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 2", "Kanal");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn4 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 3", "Build");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn5 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 4", "Öffentlich");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn6 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "Datei");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn7 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "Ort");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateManagerWindow));
            this.radRibbonBarBackstageView1 = new Telerik.WinControls.UI.RadRibbonBarBackstageView();
            this.ribbonTab_Project = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup_Project = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_NewProject = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_OpenProject = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_SaveProject = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_Options = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_ProjectOptions = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_Proxy = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_Configuration = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_UploadConfiguration = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_ExportConfiguration = new Telerik.WinControls.UI.RadButtonElement();
            this.ribbonTab_Package = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup_NewPackage = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_CretateNewPackageAndUpload = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_UploadExistingPackage = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_PackageManagement = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_EditPackageVersion = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_EditPackageDescription = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_RemovePackage = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_Discord = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_PostDiscordMessage = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_DiscordBotSettings = new Telerik.WinControls.UI.RadButtonElement();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radListView_Packages = new Telerik.WinControls.UI.RadListView();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_DownloadURL = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radWaitingBar_PackageLoading = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsRingWaitingBarIndicatorElement_PackageLoading = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            this.ribbonTab_Packaging = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup_PackagingTemplate = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement4 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement5 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_PackagingPacket = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_ExportPackage = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_PackageFiles = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_SelectFolder = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_RemoveFolder = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup_PackageExtensions = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement_ExtAdd = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement_ExtRemove = new Telerik.WinControls.UI.RadButtonElement();
            this.radPageViewPage_Packaging = new Telerik.WinControls.UI.RadPageViewPage();
            this.radTreeView_PackagingFiles = new Telerik.WinControls.UI.RadTreeView();
            this.radPageViewPage_Extensions = new Telerik.WinControls.UI.RadPageViewPage();
            this.radListView_Extensions = new Telerik.WinControls.UI.RadListView();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.radWaitingBar_PackageCreation = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsRingWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBarBackstageView1)).BeginInit();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Packages)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_DownloadURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar_PackageLoading)).BeginInit();
            this.radPageViewPage_Packaging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_PackagingFiles)).BeginInit();
            this.radPageViewPage_Extensions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Extensions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar_PackageCreation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radRibbonBarBackstageView1
            // 
            this.radRibbonBarBackstageView1.EnableKeyMap = true;
            this.radRibbonBarBackstageView1.Location = new System.Drawing.Point(0, 56);
            this.radRibbonBarBackstageView1.Name = "radRibbonBarBackstageView1";
            this.radRibbonBarBackstageView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radRibbonBarBackstageView1.SelectedItem = null;
            this.radRibbonBarBackstageView1.Size = new System.Drawing.Size(644, 491);
            this.radRibbonBarBackstageView1.TabIndex = 1;
            // 
            // ribbonTab_Project
            // 
            this.ribbonTab_Project.IsSelected = true;
            this.ribbonTab_Project.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup_Project,
            this.radRibbonBarGroup_Options,
            this.radRibbonBarGroup_Configuration});
            this.ribbonTab_Project.Name = "ribbonTab_Project";
            this.ribbonTab_Project.Text = "Projekt";
            this.ribbonTab_Project.UseMnemonic = false;
            // 
            // radRibbonBarGroup_Project
            // 
            this.radRibbonBarGroup_Project.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_NewProject,
            this.radButtonElement_OpenProject,
            this.radButtonElement_SaveProject});
            this.radRibbonBarGroup_Project.Name = "radRibbonBarGroup_Project";
            this.radRibbonBarGroup_Project.Text = "Projekt";
            // 
            // radButtonElement_NewProject
            // 
            this.radButtonElement_NewProject.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_new_file_32px;
            this.radButtonElement_NewProject.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_NewProject.Name = "radButtonElement_NewProject";
            this.radButtonElement_NewProject.Text = "<html>Neues Projekt<br />erstellen</html>";
            this.radButtonElement_NewProject.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_NewProject.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_NewProject.Click += new System.EventHandler(this.ButtonItem_NewProject_Click);
            // 
            // radButtonElement_OpenProject
            // 
            this.radButtonElement_OpenProject.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_opened_folder_32px;
            this.radButtonElement_OpenProject.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_OpenProject.Name = "radButtonElement_OpenProject";
            this.radButtonElement_OpenProject.Text = "<html>Projekt<br />öffnen</html>";
            this.radButtonElement_OpenProject.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_OpenProject.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_OpenProject.Click += new System.EventHandler(this.ButtonItem_OpenProject_Click);
            // 
            // radButtonElement_SaveProject
            // 
            this.radButtonElement_SaveProject.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_save_32px;
            this.radButtonElement_SaveProject.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_SaveProject.Name = "radButtonElement_SaveProject";
            this.radButtonElement_SaveProject.Text = "<html>Projekt<br />speichern</html>";
            this.radButtonElement_SaveProject.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_SaveProject.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_SaveProject.Click += new System.EventHandler(this.ButtonItem_SaveProject_Click);
            // 
            // radRibbonBarGroup_Options
            // 
            this.radRibbonBarGroup_Options.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_ProjectOptions,
            this.radButtonElement_Proxy});
            this.radRibbonBarGroup_Options.Name = "radRibbonBarGroup_Options";
            this.radRibbonBarGroup_Options.Text = "Optionen";
            // 
            // radButtonElement_ProjectOptions
            // 
            this.radButtonElement_ProjectOptions.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_support_32px_1;
            this.radButtonElement_ProjectOptions.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ProjectOptions.Name = "radButtonElement_ProjectOptions";
            this.radButtonElement_ProjectOptions.Text = "Projektoptionen";
            this.radButtonElement_ProjectOptions.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ProjectOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_ProjectOptions.Click += new System.EventHandler(this.ButtonItem_ProjectOptions_Click);
            // 
            // radButtonElement_Proxy
            // 
            this.radButtonElement_Proxy.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_firewall_32px;
            this.radButtonElement_Proxy.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_Proxy.Name = "radButtonElement_Proxy";
            this.radButtonElement_Proxy.Text = "Proxy";
            this.radButtonElement_Proxy.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_Proxy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_Proxy.Click += new System.EventHandler(this.ButtonItem_ProxyConfig_Click);
            // 
            // radRibbonBarGroup_Configuration
            // 
            this.radRibbonBarGroup_Configuration.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_UploadConfiguration,
            this.radButtonElement_ExportConfiguration});
            this.radRibbonBarGroup_Configuration.Name = "radRibbonBarGroup_Configuration";
            this.radRibbonBarGroup_Configuration.Text = "Konfiguration";
            // 
            // radButtonElement_UploadConfiguration
            // 
            this.radButtonElement_UploadConfiguration.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_upload_32px;
            this.radButtonElement_UploadConfiguration.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_UploadConfiguration.Name = "radButtonElement_UploadConfiguration";
            this.radButtonElement_UploadConfiguration.Text = "<html>Konfiguration<br />bereitstellen</html>";
            this.radButtonElement_UploadConfiguration.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_UploadConfiguration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_UploadConfiguration.Click += new System.EventHandler(this.ButtonItem_UploadUpdateConfiguration_Click);
            // 
            // radButtonElement_ExportConfiguration
            // 
            this.radButtonElement_ExportConfiguration.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_export_32px_3;
            this.radButtonElement_ExportConfiguration.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExportConfiguration.Name = "radButtonElement_ExportConfiguration";
            this.radButtonElement_ExportConfiguration.Text = "<html>Konfiguration<br />exportieren</html>";
            this.radButtonElement_ExportConfiguration.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExportConfiguration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_ExportConfiguration.Click += new System.EventHandler(this.ButtonItem_ExportUpdateConfiguration_Click);
            // 
            // ribbonTab_Package
            // 
            this.ribbonTab_Package.IsSelected = false;
            this.ribbonTab_Package.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup_NewPackage,
            this.radRibbonBarGroup_PackageManagement,
            this.radRibbonBarGroup_Discord});
            this.ribbonTab_Package.Name = "ribbonTab_Package";
            this.ribbonTab_Package.Text = "Pakete";
            this.ribbonTab_Package.UseMnemonic = false;
            // 
            // radRibbonBarGroup_NewPackage
            // 
            this.radRibbonBarGroup_NewPackage.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_CretateNewPackageAndUpload,
            this.radButtonElement_UploadExistingPackage});
            this.radRibbonBarGroup_NewPackage.Name = "radRibbonBarGroup_NewPackage";
            this.radRibbonBarGroup_NewPackage.Text = "Neues Paket";
            // 
            // radButtonElement_CretateNewPackageAndUpload
            // 
            this.radButtonElement_CretateNewPackageAndUpload.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_open_box_32px;
            this.radButtonElement_CretateNewPackageAndUpload.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_CretateNewPackageAndUpload.Name = "radButtonElement_CretateNewPackageAndUpload";
            this.radButtonElement_CretateNewPackageAndUpload.Text = "<html>Paket fertigstellen<br />und hochladen</html>";
            this.radButtonElement_CretateNewPackageAndUpload.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_CretateNewPackageAndUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_CretateNewPackageAndUpload.Click += new System.EventHandler(this.ButtonItem_CreateAndUploadPackage_Click);
            // 
            // radButtonElement_UploadExistingPackage
            // 
            this.radButtonElement_UploadExistingPackage.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_upload_32px;
            this.radButtonElement_UploadExistingPackage.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_UploadExistingPackage.Name = "radButtonElement_UploadExistingPackage";
            this.radButtonElement_UploadExistingPackage.Text = "<html>Vorhandenes Paket<br />hochladen</html>";
            this.radButtonElement_UploadExistingPackage.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_UploadExistingPackage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_UploadExistingPackage.Click += new System.EventHandler(this.ButtonItem_UploadExistingPackage_Click);
            // 
            // radRibbonBarGroup_PackageManagement
            // 
            this.radRibbonBarGroup_PackageManagement.Enabled = false;
            this.radRibbonBarGroup_PackageManagement.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_EditPackageVersion,
            this.radButtonElement_EditPackageDescription,
            this.radButtonElement_RemovePackage});
            this.radRibbonBarGroup_PackageManagement.Name = "radRibbonBarGroup_PackageManagement";
            this.radRibbonBarGroup_PackageManagement.Text = "Paketverwaltung";
            // 
            // radButtonElement_EditPackageVersion
            // 
            this.radButtonElement_EditPackageVersion.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_edit_property_32px;
            this.radButtonElement_EditPackageVersion.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_EditPackageVersion.Name = "radButtonElement_EditPackageVersion";
            this.radButtonElement_EditPackageVersion.Text = "<html>Version<br />ändern</html>";
            this.radButtonElement_EditPackageVersion.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_EditPackageVersion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_EditPackageVersion.Click += new System.EventHandler(this.ButtonItem_ChangeVersion_Click);
            // 
            // radButtonElement_EditPackageDescription
            // 
            this.radButtonElement_EditPackageDescription.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_edit_file_32px;
            this.radButtonElement_EditPackageDescription.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_EditPackageDescription.Name = "radButtonElement_EditPackageDescription";
            this.radButtonElement_EditPackageDescription.Text = "<html>Beschreibung<br />bearbeiten</html>";
            this.radButtonElement_EditPackageDescription.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_EditPackageDescription.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_EditPackageDescription.Click += new System.EventHandler(this.ButtonItem_EditDescription_Click);
            // 
            // radButtonElement_RemovePackage
            // 
            this.radButtonElement_RemovePackage.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_32px_1;
            this.radButtonElement_RemovePackage.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_RemovePackage.Name = "radButtonElement_RemovePackage";
            this.radButtonElement_RemovePackage.Text = "<html>Vom Server<br />löschen</html>";
            this.radButtonElement_RemovePackage.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_RemovePackage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_RemovePackage.Click += new System.EventHandler(this.ButtonItem_RemovePackage_Click);
            // 
            // radRibbonBarGroup_Discord
            // 
            this.radRibbonBarGroup_Discord.Enabled = false;
            this.radRibbonBarGroup_Discord.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_PostDiscordMessage,
            this.radButtonElement_DiscordBotSettings});
            this.radRibbonBarGroup_Discord.Name = "radRibbonBarGroup_Discord";
            this.radRibbonBarGroup_Discord.Text = "Discord";
            // 
            // radButtonElement_PostDiscordMessage
            // 
            this.radButtonElement_PostDiscordMessage.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_discord_new_logo_32px;
            this.radButtonElement_PostDiscordMessage.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_PostDiscordMessage.Name = "radButtonElement_PostDiscordMessage";
            this.radButtonElement_PostDiscordMessage.Text = "<html>Updatenachricht in<br />Discord posten</html>";
            this.radButtonElement_PostDiscordMessage.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_PostDiscordMessage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_PostDiscordMessage.Click += new System.EventHandler(this.ButtonItem_PostMsgInDiscord_Click);
            // 
            // radButtonElement_DiscordBotSettings
            // 
            this.radButtonElement_DiscordBotSettings.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_administrative_tools_32px;
            this.radButtonElement_DiscordBotSettings.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_DiscordBotSettings.Name = "radButtonElement_DiscordBotSettings";
            this.radButtonElement_DiscordBotSettings.Text = "<html>Discord-Bot-<br />Einstellungen</html>";
            this.radButtonElement_DiscordBotSettings.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_DiscordBotSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_DiscordBotSettings.Click += new System.EventHandler(this.ButtonItem_BotSettings_Click);
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.BackColor = System.Drawing.Color.Transparent;
            this.radPageViewPage1.Controls.Add(this.radListView_Packages);
            this.radPageViewPage1.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_open_box_16px;
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(124F, 33F);
            this.radPageViewPage1.Location = new System.Drawing.Point(151, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(492, 342);
            this.radPageViewPage1.Text = "Pakete";
            // 
            // radListView_Packages
            // 
            this.radListView_Packages.AllowColumnReorder = false;
            this.radListView_Packages.AllowColumnResize = false;
            this.radListView_Packages.AllowEdit = false;
            this.radListView_Packages.AllowRemove = false;
            listViewDetailColumn1.HeaderText = "Name";
            listViewDetailColumn1.Width = 170F;
            listViewDetailColumn2.HeaderText = "Version";
            listViewDetailColumn2.Width = 100F;
            listViewDetailColumn3.HeaderText = "Kanal";
            listViewDetailColumn3.Width = 70F;
            listViewDetailColumn4.HeaderText = "Build";
            listViewDetailColumn4.Width = 70F;
            listViewDetailColumn5.HeaderText = "Öffentlich";
            listViewDetailColumn5.Width = 70F;
            this.radListView_Packages.Columns.AddRange(new Telerik.WinControls.UI.ListViewDetailColumn[] {
            listViewDetailColumn1,
            listViewDetailColumn2,
            listViewDetailColumn3,
            listViewDetailColumn4,
            listViewDetailColumn5});
            this.radListView_Packages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListView_Packages.GroupItemSize = new System.Drawing.Size(200, 28);
            this.radListView_Packages.ItemSize = new System.Drawing.Size(200, 28);
            this.radListView_Packages.ItemSpacing = -1;
            this.radListView_Packages.Location = new System.Drawing.Point(0, 0);
            this.radListView_Packages.Name = "radListView_Packages";
            this.radListView_Packages.Size = new System.Drawing.Size(492, 342);
            this.radListView_Packages.TabIndex = 0;
            this.radListView_Packages.ViewType = Telerik.WinControls.UI.ListViewType.DetailsView;
            this.radListView_Packages.SelectedItemChanged += new System.EventHandler(this.RadListView_Packages_SelectedItemChanged);
            this.radListView_Packages.CellFormatting += new Telerik.WinControls.UI.ListViewCellFormattingEventHandler(this.RadListView1_CellFormatting);
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.BackColor = System.Drawing.Color.Transparent;
            this.radPageViewPage2.Controls.Add(this.panel1);
            this.radPageViewPage2.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_software_installer_16px;
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(124F, 33F);
            this.radPageViewPage2.Location = new System.Drawing.Point(41, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(602, 340);
            this.radPageViewPage2.Text = "Update-Installer";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBoxControl_DownloadURL);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(602, 340);
            this.panel1.TabIndex = 2;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(84, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Download-URL:";
            // 
            // radTextBoxControl_DownloadURL
            // 
            this.radTextBoxControl_DownloadURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_DownloadURL.Location = new System.Drawing.Point(3, 27);
            this.radTextBoxControl_DownloadURL.Name = "radTextBoxControl_DownloadURL";
            this.radTextBoxControl_DownloadURL.Size = new System.Drawing.Size(596, 22);
            this.radTextBoxControl_DownloadURL.TabIndex = 1;
            this.radTextBoxControl_DownloadURL.TextChanged += new System.EventHandler(this.TextBoxX_UpdateInstallerDownloadUrl_TextChanged);
            // 
            // radWaitingBar_PackageLoading
            // 
            this.radWaitingBar_PackageLoading.AssociatedControl = this.radListView_Packages;
            this.radWaitingBar_PackageLoading.Location = new System.Drawing.Point(341, 325);
            this.radWaitingBar_PackageLoading.Name = "radWaitingBar_PackageLoading";
            this.radWaitingBar_PackageLoading.Size = new System.Drawing.Size(70, 70);
            this.radWaitingBar_PackageLoading.TabIndex = 3;
            this.radWaitingBar_PackageLoading.Text = "radWaitingBar1";
            this.radWaitingBar_PackageLoading.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement_PackageLoading);
            this.radWaitingBar_PackageLoading.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar_PackageLoading.WaitingSpeed = 50;
            this.radWaitingBar_PackageLoading.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            // 
            // dotsRingWaitingBarIndicatorElement_PackageLoading
            // 
            this.dotsRingWaitingBarIndicatorElement_PackageLoading.Name = "dotsRingWaitingBarIndicatorElement_PackageLoading";
            // 
            // ribbonTab_Packaging
            // 
            this.ribbonTab_Packaging.IsSelected = false;
            this.ribbonTab_Packaging.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup_PackagingTemplate,
            this.radRibbonBarGroup_PackagingPacket,
            this.radRibbonBarGroup_PackageFiles,
            this.radRibbonBarGroup_PackageExtensions});
            this.ribbonTab_Packaging.Name = "ribbonTab_Packaging";
            this.ribbonTab_Packaging.Text = "Paketierung";
            this.ribbonTab_Packaging.UseMnemonic = false;
            // 
            // radRibbonBarGroup_PackagingTemplate
            // 
            this.radRibbonBarGroup_PackagingTemplate.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1,
            this.radButtonElement2,
            this.radButtonElement4,
            this.radButtonElement5});
            this.radRibbonBarGroup_PackagingTemplate.Name = "radRibbonBarGroup_PackagingTemplate";
            this.radRibbonBarGroup_PackagingTemplate.Text = "Vorlage";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_new_file_32px;
            this.radButtonElement1.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement1.Name = "radButtonElement1";
            this.radButtonElement1.Text = "<html>Neue<br />Vorlage</html>";
            this.radButtonElement1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement1.Click += new System.EventHandler(this.ButtonItem_Pkg_NewTemplate_Click);
            // 
            // radButtonElement2
            // 
            this.radButtonElement2.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_template_32px;
            this.radButtonElement2.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement2.Name = "radButtonElement2";
            this.radButtonElement2.Text = "<html>Vorlage<br />öffnen</html>";
            this.radButtonElement2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement2.Click += new System.EventHandler(this.ButtonItem_Pkg_OpenTemplate_Click);
            // 
            // radButtonElement4
            // 
            this.radButtonElement4.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_save_32px;
            this.radButtonElement4.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement4.Name = "radButtonElement4";
            this.radButtonElement4.Text = "<html>Vorlage<br />speichern</html>";
            this.radButtonElement4.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement4.Click += new System.EventHandler(this.ButtonItem_Pkg_SaveTemplate_Click);
            // 
            // radButtonElement5
            // 
            this.radButtonElement5.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_save_as_32px;
            this.radButtonElement5.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement5.Name = "radButtonElement5";
            this.radButtonElement5.Text = "<html>Speichern<br />unter</html>";
            this.radButtonElement5.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement5.Click += new System.EventHandler(this.ButtonItem_Pkg_SaveTemplateAs_Click);
            // 
            // radRibbonBarGroup_PackagingPacket
            // 
            this.radRibbonBarGroup_PackagingPacket.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_ExportPackage});
            this.radRibbonBarGroup_PackagingPacket.Name = "radRibbonBarGroup_PackagingPacket";
            this.radRibbonBarGroup_PackagingPacket.Text = "Paket";
            // 
            // radButtonElement_ExportPackage
            // 
            this.radButtonElement_ExportPackage.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_export_32px_3;
            this.radButtonElement_ExportPackage.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExportPackage.Name = "radButtonElement_ExportPackage";
            this.radButtonElement_ExportPackage.Text = "<html>Paket<br />exportieren</html>";
            this.radButtonElement_ExportPackage.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExportPackage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_ExportPackage.Click += new System.EventHandler(this.ButtonItem_Pkg_Export_Click);
            // 
            // radRibbonBarGroup_PackageFiles
            // 
            this.radRibbonBarGroup_PackageFiles.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_SelectFolder,
            this.radButtonElement_RemoveFolder});
            this.radRibbonBarGroup_PackageFiles.Name = "radRibbonBarGroup_PackageFiles";
            this.radRibbonBarGroup_PackageFiles.Text = "Dateien";
            this.radRibbonBarGroup_PackageFiles.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // radButtonElement_SelectFolder
            // 
            this.radButtonElement_SelectFolder.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_opened_folder_32px;
            this.radButtonElement_SelectFolder.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_SelectFolder.Name = "radButtonElement_SelectFolder";
            this.radButtonElement_SelectFolder.Text = "<html>Ordner<br />auswählen</html>";
            this.radButtonElement_SelectFolder.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_SelectFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_SelectFolder.Click += new System.EventHandler(this.ButtonItem_Pkg_SelectFileFolder_Click);
            // 
            // radButtonElement_RemoveFolder
            // 
            this.radButtonElement_RemoveFolder.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_32px_1;
            this.radButtonElement_RemoveFolder.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_RemoveFolder.Name = "radButtonElement_RemoveFolder";
            this.radButtonElement_RemoveFolder.Text = "<html>Ordner<br />entfernen</html>";
            this.radButtonElement_RemoveFolder.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_RemoveFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_RemoveFolder.Click += new System.EventHandler(this.ButtonItem_Pkg_RemoveFileFolder_Click);
            // 
            // radRibbonBarGroup_PackageExtensions
            // 
            this.radRibbonBarGroup_PackageExtensions.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_ExtAdd,
            this.radButtonElement_ExtRemove});
            this.radRibbonBarGroup_PackageExtensions.Name = "radRibbonBarGroup_PackageExtensions";
            this.radRibbonBarGroup_PackageExtensions.Text = "Erweiterungen";
            this.radRibbonBarGroup_PackageExtensions.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // radButtonElement_ExtAdd
            // 
            this.radButtonElement_ExtAdd.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_merge_files_32px;
            this.radButtonElement_ExtAdd.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExtAdd.Name = "radButtonElement_ExtAdd";
            this.radButtonElement_ExtAdd.Text = "<html>Erweiterung<br />hinzufügen</html>";
            this.radButtonElement_ExtAdd.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExtAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_ExtAdd.Click += new System.EventHandler(this.ButtonItem_Pkg_AddExtension_Click);
            // 
            // radButtonElement_ExtRemove
            // 
            this.radButtonElement_ExtRemove.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_32px_1;
            this.radButtonElement_ExtRemove.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExtRemove.Name = "radButtonElement_ExtRemove";
            this.radButtonElement_ExtRemove.Text = "<html>Erweiterung<br />entfernen</html>";
            this.radButtonElement_ExtRemove.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement_ExtRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement_ExtRemove.Click += new System.EventHandler(this.ButtonItem_Pkg_RemoveExtension_Click);
            // 
            // radPageViewPage_Packaging
            // 
            this.radPageViewPage_Packaging.Controls.Add(this.radTreeView_PackagingFiles);
            this.radPageViewPage_Packaging.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_new_file_16px;
            this.radPageViewPage_Packaging.ItemSize = new System.Drawing.SizeF(124F, 33F);
            this.radPageViewPage_Packaging.Location = new System.Drawing.Point(151, 37);
            this.radPageViewPage_Packaging.Name = "radPageViewPage_Packaging";
            this.radPageViewPage_Packaging.Size = new System.Drawing.Size(492, 340);
            this.radPageViewPage_Packaging.Text = "Paketierung";
            // 
            // radTreeView_PackagingFiles
            // 
            this.radTreeView_PackagingFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_PackagingFiles.ItemHeight = 28;
            this.radTreeView_PackagingFiles.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_PackagingFiles.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_PackagingFiles.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_PackagingFiles.Name = "radTreeView_PackagingFiles";
            this.radTreeView_PackagingFiles.Size = new System.Drawing.Size(492, 340);
            this.radTreeView_PackagingFiles.TabIndex = 0;
            // 
            // radPageViewPage_Extensions
            // 
            this.radPageViewPage_Extensions.Controls.Add(this.radListView_Extensions);
            this.radPageViewPage_Extensions.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_merge_files_16px_1;
            this.radPageViewPage_Extensions.ItemSize = new System.Drawing.SizeF(124F, 33F);
            this.radPageViewPage_Extensions.Location = new System.Drawing.Point(151, 37);
            this.radPageViewPage_Extensions.Name = "radPageViewPage_Extensions";
            this.radPageViewPage_Extensions.Size = new System.Drawing.Size(492, 340);
            this.radPageViewPage_Extensions.Text = "Erweiterungen";
            // 
            // radListView_Extensions
            // 
            listViewDetailColumn6.HeaderText = "Datei";
            listViewDetailColumn6.Width = 150F;
            listViewDetailColumn7.HeaderText = "Ort";
            listViewDetailColumn7.Width = 250F;
            this.radListView_Extensions.Columns.AddRange(new Telerik.WinControls.UI.ListViewDetailColumn[] {
            listViewDetailColumn6,
            listViewDetailColumn7});
            this.radListView_Extensions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListView_Extensions.GroupItemSize = new System.Drawing.Size(200, 28);
            this.radListView_Extensions.ItemSize = new System.Drawing.Size(200, 28);
            this.radListView_Extensions.ItemSpacing = -1;
            this.radListView_Extensions.Location = new System.Drawing.Point(0, 0);
            this.radListView_Extensions.Name = "radListView_Extensions";
            this.radListView_Extensions.Size = new System.Drawing.Size(492, 340);
            this.radListView_Extensions.TabIndex = 0;
            this.radListView_Extensions.ViewType = Telerik.WinControls.UI.ListViewType.DetailsView;
            this.radListView_Extensions.CellFormatting += new Telerik.WinControls.UI.ListViewCellFormattingEventHandler(this.RadListView1_CellFormatting_1);
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage_Packaging);
            this.radPageView1.Controls.Add(this.radPageViewPage_Extensions);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 167);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(644, 380);
            this.radPageView1.TabIndex = 2;
            this.radPageView1.ViewMode = Telerik.WinControls.UI.PageViewMode.NavigationView;
            this.radPageView1.SelectedPageChanged += new System.EventHandler(this.RadPageView1_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewNavigationViewElement)(radPageView1.GetChildAt(0))).HeaderHeight = 36;
            ((Telerik.WinControls.UI.RadPageViewNavigationViewElement)(radPageView1.GetChildAt(0))).ExpandedPaneWidth = 150;
            ((Telerik.WinControls.UI.RadPageViewNavigationViewElement)(radPageView1.GetChildAt(0))).CollapsedPaneWidth = 40;
            ((Telerik.WinControls.UI.RadPageViewNavigationViewElement)(radPageView1.GetChildAt(0))).ItemFitMode = Telerik.WinControls.UI.StripViewItemFitMode.FillHeight;
            ((Telerik.WinControls.UI.RadPageViewNavigationViewElement)(radPageView1.GetChildAt(0))).ItemSizeMode = Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight;
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.BackstageControl = this.radRibbonBarBackstageView1;
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab_Project,
            this.ribbonTab_Package,
            this.ribbonTab_Packaging});
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            this.radRibbonBar1.ShowExpandButton = false;
            this.radRibbonBar1.Size = new System.Drawing.Size(644, 167);
            this.radRibbonBar1.StartButtonImage = null;
            this.radRibbonBar1.TabIndex = 0;
            this.radRibbonBar1.Text = "Administration";
            ((Telerik.WinControls.UI.RadRibbonBarElement)(radRibbonBar1.GetChildAt(0))).Text = "Administration";
            ((Telerik.WinControls.UI.RadQuickAccessToolBar)(radRibbonBar1.GetChildAt(0).GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadApplicationMenuButtonElement)(radRibbonBar1.GetChildAt(0).GetChildAt(5))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // radWaitingBar_PackageCreation
            // 
            this.radWaitingBar_PackageCreation.AssociatedControl = this.radTreeView_PackagingFiles;
            this.radWaitingBar_PackageCreation.Location = new System.Drawing.Point(414, 323);
            this.radWaitingBar_PackageCreation.Name = "radWaitingBar_PackageCreation";
            this.radWaitingBar_PackageCreation.Size = new System.Drawing.Size(70, 70);
            this.radWaitingBar_PackageCreation.TabIndex = 4;
            this.radWaitingBar_PackageCreation.Text = "radWaitingBar1";
            this.radWaitingBar_PackageCreation.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement1);
            this.radWaitingBar_PackageCreation.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar_PackageCreation.WaitingSpeed = 50;
            this.radWaitingBar_PackageCreation.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            // 
            // dotsRingWaitingBarIndicatorElement1
            // 
            this.dotsRingWaitingBarIndicatorElement1.Name = "dotsRingWaitingBarIndicatorElement1";
            // 
            // UpdateManagerWindow
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 547);
            this.Controls.Add(this.radWaitingBar_PackageCreation);
            this.Controls.Add(this.radWaitingBar_PackageLoading);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.radRibbonBar1);
            this.Controls.Add(this.radRibbonBarBackstageView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateManagerWindow";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "650; 550";
            this.Text = "Administration";
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBarBackstageView1)).EndInit();
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Packages)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_DownloadURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar_PackageLoading)).EndInit();
            this.radPageViewPage_Packaging.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_PackagingFiles)).EndInit();
            this.radPageViewPage_Extensions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Extensions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar_PackageCreation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RadRibbonBarBackstageView radRibbonBarBackstageView1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab_Project;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_Project;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_NewProject;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_Options;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_Configuration;
        private Telerik.WinControls.UI.RibbonTab ribbonTab_Package;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_OpenProject;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_SaveProject;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ProjectOptions;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Proxy;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_UploadConfiguration;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExportConfiguration;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_NewPackage;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_CretateNewPackageAndUpload;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_UploadExistingPackage;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_PackageManagement;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_EditPackageVersion;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_EditPackageDescription;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_RemovePackage;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_Discord;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_PostDiscordMessage;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_DiscordBotSettings;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadListView radListView_Packages;
        private Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_DownloadURL;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar_PackageLoading;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement_PackageLoading;
        private Telerik.WinControls.UI.RibbonTab ribbonTab_Packaging;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_PackagingTemplate;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement5;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_PackagingPacket;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExportPackage;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_PackageFiles;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_SelectFolder;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_RemoveFolder;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup_PackageExtensions;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExtAdd;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExtRemove;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage_Packaging;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage_Extensions;
        private Telerik.WinControls.UI.RadTreeView radTreeView_PackagingFiles;
        private Telerik.WinControls.UI.RadListView radListView_Extensions;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar_PackageCreation;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement1;
    }
}