﻿namespace Pilz.Updating.Administration.GUI
{
    partial class UpdateServerInfoEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateServerInfoEditor));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_ServerAddress = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBoxControl_PublicBasisURL = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBoxControl_PackageConfigurationFileName = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBoxControl_Username = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBoxControl_Password = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radToggleSwitch_UseProxyForWebDAV = new Telerik.WinControls.UI.RadToggleSwitch();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_ServerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_PublicBasisURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_PackageConfigurationFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Username)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UseProxyForWebDAV)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(91, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Server-Adresse:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(126, 19);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Öffentliche Basis-URL:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 61);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(176, 19);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Paketkonfigurationsdateiname:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(3, 89);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(88, 19);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Benutzername:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(3, 117);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(58, 19);
            this.radLabel5.TabIndex = 4;
            this.radLabel5.Text = "Passwort:";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(3, 145);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(158, 19);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Benutze Proxy für WebDAV:";
            // 
            // radTextBoxControl_ServerAddress
            // 
            this.radTextBoxControl_ServerAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_ServerAddress.Location = new System.Drawing.Point(185, 3);
            this.radTextBoxControl_ServerAddress.Name = "radTextBoxControl_ServerAddress";
            this.radTextBoxControl_ServerAddress.Size = new System.Drawing.Size(311, 22);
            this.radTextBoxControl_ServerAddress.TabIndex = 6;
            // 
            // radTextBoxControl_PublicBasisURL
            // 
            this.radTextBoxControl_PublicBasisURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_PublicBasisURL.Location = new System.Drawing.Point(185, 31);
            this.radTextBoxControl_PublicBasisURL.Name = "radTextBoxControl_PublicBasisURL";
            this.radTextBoxControl_PublicBasisURL.Size = new System.Drawing.Size(311, 22);
            this.radTextBoxControl_PublicBasisURL.TabIndex = 7;
            // 
            // radTextBoxControl_PackageConfigurationFileName
            // 
            this.radTextBoxControl_PackageConfigurationFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_PackageConfigurationFileName.Location = new System.Drawing.Point(185, 59);
            this.radTextBoxControl_PackageConfigurationFileName.Name = "radTextBoxControl_PackageConfigurationFileName";
            this.radTextBoxControl_PackageConfigurationFileName.Size = new System.Drawing.Size(311, 22);
            this.radTextBoxControl_PackageConfigurationFileName.TabIndex = 8;
            // 
            // radTextBoxControl_Username
            // 
            this.radTextBoxControl_Username.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Username.Location = new System.Drawing.Point(185, 87);
            this.radTextBoxControl_Username.Name = "radTextBoxControl_Username";
            this.radTextBoxControl_Username.Size = new System.Drawing.Size(311, 22);
            this.radTextBoxControl_Username.TabIndex = 9;
            // 
            // radTextBoxControl_Password
            // 
            this.radTextBoxControl_Password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Password.Location = new System.Drawing.Point(185, 115);
            this.radTextBoxControl_Password.Name = "radTextBoxControl_Password";
            this.radTextBoxControl_Password.Size = new System.Drawing.Size(311, 22);
            this.radTextBoxControl_Password.TabIndex = 10;
            this.radTextBoxControl_Password.UseSystemPasswordChar = true;
            // 
            // radToggleSwitch_UseProxyForWebDAV
            // 
            this.radToggleSwitch_UseProxyForWebDAV.Location = new System.Drawing.Point(185, 144);
            this.radToggleSwitch_UseProxyForWebDAV.Name = "radToggleSwitch_UseProxyForWebDAV";
            this.radToggleSwitch_UseProxyForWebDAV.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitch_UseProxyForWebDAV.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radToggleSwitch_UseProxyForWebDAV);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radTextBoxControl_Password);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radTextBoxControl_Username);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radTextBoxControl_PackageConfigurationFileName);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radTextBoxControl_PublicBasisURL);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radTextBoxControl_ServerAddress);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(499, 197);
            this.panel1.TabIndex = 12;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_checkmark_16px;
            this.radButton_Save.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Save.Location = new System.Drawing.Point(396, 170);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(100, 24);
            this.radButton_Save.TabIndex = 13;
            this.radButton_Save.Text = "Speichern";
            this.radButton_Save.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton_Save.Click += new System.EventHandler(this.ButtonX_OK_Click);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_16px;
            this.radButton_Cancel.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Cancel.Location = new System.Drawing.Point(290, 170);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(100, 24);
            this.radButton_Cancel.TabIndex = 12;
            this.radButton_Cancel.Text = "Abbrechen";
            this.radButton_Cancel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // UpdateServerInfoEditor
            // 
            this.AcceptButton = this.radButton_Save;
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(499, 197);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "UpdateServerInfoEditor";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UpdateServerInfoEditor";
            this.Shown += new System.EventHandler(this.UpdateServerInfoEditor_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_ServerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_PublicBasisURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_PackageConfigurationFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Username)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UseProxyForWebDAV)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_ServerAddress;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_PublicBasisURL;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_PackageConfigurationFileName;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Username;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Password;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_UseProxyForWebDAV;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
    }
}