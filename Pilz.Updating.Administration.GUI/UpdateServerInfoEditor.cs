﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Administration.GUI
{
    public partial class UpdateServerInfoEditor : RadForm
    {
        public UpdateServerInfoEditor()
        {
            InitializeComponent();
        }

        private void UpdateServerInfoEditor_Shown(object sender, EventArgs e)
        {
            radTextBoxControl_ServerAddress.Text = General.CurProject.UpdateServerConfig.ServerAdress;
            radTextBoxControl_PublicBasisURL.Text = General.CurProject.UpdateServerConfig.PublicPackageBaseURL;
            radTextBoxControl_PackageConfigurationFileName.Text = General.CurProject.UpdateServerConfig.UpdateInfoFilename;
            radTextBoxControl_Username.Text = General.CurProject.UpdateServerConfig.Username;
            radTextBoxControl_Password.Text = General.CurProject.UpdateServerConfig.Password;
            radToggleSwitch_UseProxyForWebDAV.Value = General.CurProject.UpdateServerConfig.UseProxyForWebDAV;
        }

        private void ButtonX_OK_Click(object sender, EventArgs e)
        {
            General.CurProject.UpdateServerConfig.ServerAdress = radTextBoxControl_ServerAddress.Text.Trim();
            General.CurProject.UpdateServerConfig.PublicPackageBaseURL  = radTextBoxControl_PublicBasisURL.Text.Trim();
            General.CurProject.UpdateServerConfig.UpdateInfoFilename = radTextBoxControl_PackageConfigurationFileName.Text.Trim();
            General.CurProject.UpdateServerConfig.Username = radTextBoxControl_Username.Text.Trim();
            General.CurProject.UpdateServerConfig.Password = radTextBoxControl_Password.Text;
            General.CurProject.UpdateServerConfig.UseProxyForWebDAV = radToggleSwitch_UseProxyForWebDAV.Value;
        }
    }
}
