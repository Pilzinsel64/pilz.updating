namespace Pilz.Updating.Administration.GUI
{
    partial class DiscordBotSettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscordBotSettingsWindow));
            this.panel1 = new System.Windows.Forms.Panel();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_PresetChannel = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_DefaultMessage = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_BotToken = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_DefaultProgramName = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch_UseProxy = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radButton_Abbrechen = new Telerik.WinControls.UI.RadButton();
            this.radButton_Okay = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PresetChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DefaultMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_BotToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_DefaultProgramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UseProxy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Abbrechen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Okay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radSeparator1);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radDropDownList_PresetChannel);
            this.panel1.Controls.Add(this.radTextBox_DefaultMessage);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radTextBoxControl_BotToken);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radTextBoxControl_DefaultProgramName);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radToggleSwitch_UseProxy);
            this.panel1.Controls.Add(this.radButton_Abbrechen);
            this.panel1.Controls.Add(this.radButton_Okay);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(736, 471);
            this.panel1.TabIndex = 0;
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(3, 86);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(730, 4);
            this.radSeparator1.TabIndex = 8;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(3, 97);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(86, 19);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Kanal-Vorlage:";
            // 
            // radDropDownList_PresetChannel
            // 
            this.radDropDownList_PresetChannel.DropDownAnimationEnabled = true;
            this.radDropDownList_PresetChannel.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_PresetChannel.Location = new System.Drawing.Point(127, 96);
            this.radDropDownList_PresetChannel.Name = "radDropDownList_PresetChannel";
            this.radDropDownList_PresetChannel.Size = new System.Drawing.Size(144, 22);
            this.radDropDownList_PresetChannel.TabIndex = 1;
            this.radDropDownList_PresetChannel.SelectedValueChanged += new System.EventHandler(this.radDropDownList_PresetChannel_SelectedValueChanged);
            // 
            // radTextBox_DefaultMessage
            // 
            this.radTextBox_DefaultMessage.AcceptsReturn = true;
            this.radTextBox_DefaultMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_DefaultMessage.Location = new System.Drawing.Point(127, 124);
            this.radTextBox_DefaultMessage.Multiline = true;
            this.radTextBox_DefaultMessage.Name = "radTextBox_DefaultMessage";
            // 
            // 
            // 
            this.radTextBox_DefaultMessage.RootElement.StretchVertically = true;
            this.radTextBox_DefaultMessage.Size = new System.Drawing.Size(606, 314);
            this.radTextBox_DefaultMessage.TabIndex = 6;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(3, 126);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(113, 19);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Standart-Nachricht:";
            // 
            // radTextBoxControl_BotToken
            // 
            this.radTextBoxControl_BotToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_BotToken.Location = new System.Drawing.Point(127, 3);
            this.radTextBoxControl_BotToken.Name = "radTextBoxControl_BotToken";
            this.radTextBoxControl_BotToken.Size = new System.Drawing.Size(606, 22);
            this.radTextBoxControl_BotToken.TabIndex = 0;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 60);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(124, 19);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Standart-Programme:";
            // 
            // radTextBoxControl_DefaultProgramName
            // 
            this.radTextBoxControl_DefaultProgramName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_DefaultProgramName.Location = new System.Drawing.Point(127, 58);
            this.radTextBoxControl_DefaultProgramName.Name = "radTextBoxControl_DefaultProgramName";
            this.radTextBoxControl_DefaultProgramName.Size = new System.Drawing.Size(606, 22);
            this.radTextBoxControl_DefaultProgramName.TabIndex = 4;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(102, 19);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Proxy verwenden:";
            // 
            // radToggleSwitch_UseProxy
            // 
            this.radToggleSwitch_UseProxy.Location = new System.Drawing.Point(127, 32);
            this.radToggleSwitch_UseProxy.Name = "radToggleSwitch_UseProxy";
            this.radToggleSwitch_UseProxy.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitch_UseProxy.TabIndex = 0;
            // 
            // radButton_Abbrechen
            // 
            this.radButton_Abbrechen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Abbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Abbrechen.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_16px;
            this.radButton_Abbrechen.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Abbrechen.Location = new System.Drawing.Point(527, 444);
            this.radButton_Abbrechen.Name = "radButton_Abbrechen";
            this.radButton_Abbrechen.Size = new System.Drawing.Size(100, 24);
            this.radButton_Abbrechen.TabIndex = 0;
            this.radButton_Abbrechen.Text = "Abbrechen";
            this.radButton_Abbrechen.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Abbrechen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButton_Okay
            // 
            this.radButton_Okay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Okay.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.radButton_Okay.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_checkmark_16px;
            this.radButton_Okay.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Okay.Location = new System.Drawing.Point(633, 444);
            this.radButton_Okay.Name = "radButton_Okay";
            this.radButton_Okay.Size = new System.Drawing.Size(100, 24);
            this.radButton_Okay.TabIndex = 5;
            this.radButton_Okay.Text = "Okay";
            this.radButton_Okay.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Okay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton_Okay.Click += new System.EventHandler(this.ButtonX_Okay_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(66, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Bot-Token:";
            // 
            // DiscordBotSettingsWindow
            // 
            this.CancelButton = this.radButton_Abbrechen;
            this.ClientSize = new System.Drawing.Size(736, 471);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DiscordBotSettingsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Discord Bot-Einstellungen";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PresetChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DefaultMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_BotToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_DefaultProgramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UseProxy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Abbrechen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Okay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_BotToken;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_DefaultProgramName;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_UseProxy;
        private Telerik.WinControls.UI.RadButton radButton_Abbrechen;
        private Telerik.WinControls.UI.RadButton radButton_Okay;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DefaultMessage;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_PresetChannel;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
    }
}