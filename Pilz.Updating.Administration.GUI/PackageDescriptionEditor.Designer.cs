﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace Pilz.Updating.Administration.GUI
{
    [DesignerGenerated()]
    public partial class PackageDescriptionEditor : Telerik.WinControls.UI.RadForm
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = new System.ComponentModel.Container();

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackageDescriptionEditor));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_Titel = new Telerik.WinControls.UI.RadTextBoxControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_Description = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList_Formatting = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Okay = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Titel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Formatting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Okay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(32, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Titel:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(83, 19);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Beschreibung:";
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel3.Location = new System.Drawing.Point(3, 241);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(83, 19);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Formatierung:";
            // 
            // radTextBoxControl_Titel
            // 
            this.radTextBoxControl_Titel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Titel.Location = new System.Drawing.Point(89, 3);
            this.radTextBoxControl_Titel.Name = "radTextBoxControl_Titel";
            this.radTextBoxControl_Titel.Size = new System.Drawing.Size(292, 22);
            this.radTextBoxControl_Titel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radTextBox_Description);
            this.panel1.Controls.Add(this.radDropDownList_Formatting);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radButton_Okay);
            this.panel1.Controls.Add(this.radTextBoxControl_Titel);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 294);
            this.panel1.TabIndex = 4;
            // 
            // radTextBox_Description
            // 
            this.radTextBox_Description.AcceptsReturn = true;
            this.radTextBox_Description.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Description.Location = new System.Drawing.Point(89, 31);
            this.radTextBox_Description.Multiline = true;
            this.radTextBox_Description.Name = "radTextBox_Description";
            // 
            // 
            // 
            this.radTextBox_Description.RootElement.StretchVertically = true;
            this.radTextBox_Description.Size = new System.Drawing.Size(292, 202);
            this.radTextBox_Description.TabIndex = 7;
            // 
            // radDropDownList_Formatting
            // 
            this.radDropDownList_Formatting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radDropDownList_Formatting.DropDownAnimationEnabled = true;
            this.radDropDownList_Formatting.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Formatting.Location = new System.Drawing.Point(89, 241);
            this.radDropDownList_Formatting.Name = "radDropDownList_Formatting";
            this.radDropDownList_Formatting.Size = new System.Drawing.Size(292, 22);
            this.radDropDownList_Formatting.TabIndex = 6;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_16px;
            this.radButton_Cancel.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Cancel.Location = new System.Drawing.Point(175, 267);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(100, 24);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "Abbrechen";
            this.radButton_Cancel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButton_Okay
            // 
            this.radButton_Okay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Okay.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.radButton_Okay.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_checkmark_16px;
            this.radButton_Okay.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Okay.Location = new System.Drawing.Point(281, 267);
            this.radButton_Okay.Name = "radButton_Okay";
            this.radButton_Okay.Size = new System.Drawing.Size(100, 24);
            this.radButton_Okay.TabIndex = 4;
            this.radButton_Okay.Text = "Okay";
            this.radButton_Okay.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Okay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // PackageDescriptionEditor
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(384, 294);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PackageDescriptionEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Beschreibung";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Titel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Formatting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Okay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Titel;
        private Panel panel1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Formatting;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadButton radButton_Okay;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Description;
    }
}