﻿namespace Pilz.Updating.Administration.GUI
{
    partial class DiscordPostDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscordPostDialog));
            this.radTreeView_Channels = new Telerik.WinControls.UI.RadTreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_Message = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_ProgramName = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radCheckBoxPingAtEveryone = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_AttachDescription = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Send = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_Channels)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Message)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_ProgramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxPingAtEveryone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_AttachDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Send)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTreeView_Channels
            // 
            this.radTreeView_Channels.Dock = System.Windows.Forms.DockStyle.Left;
            this.radTreeView_Channels.ItemHeight = 28;
            this.radTreeView_Channels.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_Channels.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_Channels.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_Channels.Name = "radTreeView_Channels";
            this.radTreeView_Channels.Size = new System.Drawing.Size(270, 461);
            this.radTreeView_Channels.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radTextBox_Message);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radTextBoxControl_ProgramName);
            this.panel1.Controls.Add(this.radCheckBoxPingAtEveryone);
            this.panel1.Controls.Add(this.radCheckBox_AttachDescription);
            this.panel1.Controls.Add(this.radButton_Send);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(270, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 461);
            this.panel1.TabIndex = 1;
            // 
            // radTextBox_Message
            // 
            this.radTextBox_Message.AcceptsReturn = true;
            this.radTextBox_Message.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Message.Location = new System.Drawing.Point(101, 31);
            this.radTextBox_Message.Multiline = true;
            this.radTextBox_Message.Name = "radTextBox_Message";
            // 
            // 
            // 
            this.radTextBox_Message.RootElement.StretchVertically = true;
            this.radTextBox_Message.Size = new System.Drawing.Size(410, 397);
            this.radTextBox_Message.TabIndex = 4;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(6, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(62, 19);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Nachricht:";
            // 
            // radTextBoxControl_ProgramName
            // 
            this.radTextBoxControl_ProgramName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_ProgramName.Location = new System.Drawing.Point(101, 3);
            this.radTextBoxControl_ProgramName.Name = "radTextBoxControl_ProgramName";
            this.radTextBoxControl_ProgramName.Size = new System.Drawing.Size(410, 22);
            this.radTextBoxControl_ProgramName.TabIndex = 0;
            // 
            // radCheckBoxPingAtEveryone
            // 
            this.radCheckBoxPingAtEveryone.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBoxPingAtEveryone.Location = new System.Drawing.Point(155, 437);
            this.radCheckBoxPingAtEveryone.Name = "radCheckBoxPingAtEveryone";
            this.radCheckBoxPingAtEveryone.Size = new System.Drawing.Size(111, 17);
            this.radCheckBoxPingAtEveryone.TabIndex = 0;
            this.radCheckBoxPingAtEveryone.Text = "Ping @everyone";
            this.radCheckBoxPingAtEveryone.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBox_AttachDescription
            // 
            this.radCheckBox_AttachDescription.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox_AttachDescription.Location = new System.Drawing.Point(272, 437);
            this.radCheckBox_AttachDescription.Name = "radCheckBox_AttachDescription";
            this.radCheckBox_AttachDescription.Size = new System.Drawing.Size(153, 17);
            this.radCheckBox_AttachDescription.TabIndex = 3;
            this.radCheckBox_AttachDescription.Text = "Beschreibung anhängen";
            this.radCheckBox_AttachDescription.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radButton_Send
            // 
            this.radButton_Send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Send.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_email_send_16px;
            this.radButton_Send.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Send.Location = new System.Drawing.Point(431, 434);
            this.radButton_Send.Name = "radButton_Send";
            this.radButton_Send.Size = new System.Drawing.Size(80, 24);
            this.radButton_Send.TabIndex = 0;
            this.radButton_Send.Text = "Senden";
            this.radButton_Send.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Send.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton_Send.Click += new System.EventHandler(this.ButtonX_SendMsg_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(6, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(97, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Programmname:";
            // 
            // DiscordPostDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radTreeView_Channels);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DiscordPostDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Aktuallisierungspost auf Discord";
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_Channels)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Message)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_ProgramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxPingAtEveryone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_AttachDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Send)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTreeView radTreeView_Channels;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxPingAtEveryone;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_AttachDescription;
        private Telerik.WinControls.UI.RadButton radButton_Send;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_ProgramName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Message;
    }
}