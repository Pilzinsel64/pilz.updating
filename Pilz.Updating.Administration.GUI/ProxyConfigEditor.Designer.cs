﻿namespace Pilz.Updating.Administration.GUI
{
    partial class ProxyConfigEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProxyConfigEditor));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch_UserProxy = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radTextBoxControl_Username = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radTextBoxControl_Password = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_Accept = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UserProxy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Username)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Accept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(139, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Proxy-Authentifizierung:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(88, 19);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Benutzername:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 61);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(58, 19);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Passwort:";
            // 
            // radToggleSwitch_UserProxy
            // 
            this.radToggleSwitch_UserProxy.Location = new System.Drawing.Point(145, 4);
            this.radToggleSwitch_UserProxy.Name = "radToggleSwitch_UserProxy";
            this.radToggleSwitch_UserProxy.Size = new System.Drawing.Size(50, 20);
            this.radToggleSwitch_UserProxy.TabIndex = 0;
            // 
            // radTextBoxControl_Username
            // 
            this.radTextBoxControl_Username.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Username.Location = new System.Drawing.Point(145, 31);
            this.radTextBoxControl_Username.Name = "radTextBoxControl_Username";
            this.radTextBoxControl_Username.Size = new System.Drawing.Size(351, 22);
            this.radTextBoxControl_Username.TabIndex = 0;
            // 
            // radTextBoxControl_Password
            // 
            this.radTextBoxControl_Password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Password.Location = new System.Drawing.Point(145, 59);
            this.radTextBoxControl_Password.Name = "radTextBoxControl_Password";
            this.radTextBoxControl_Password.Size = new System.Drawing.Size(351, 22);
            this.radTextBoxControl_Password.TabIndex = 3;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_16px;
            this.radButton_Cancel.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Cancel.Location = new System.Drawing.Point(290, 87);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(100, 24);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "Abbrechen";
            this.radButton_Cancel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radToggleSwitch_UserProxy);
            this.panel1.Controls.Add(this.radTextBoxControl_Username);
            this.panel1.Controls.Add(this.radButton_Accept);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radTextBoxControl_Password);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(499, 114);
            this.panel1.TabIndex = 5;
            // 
            // radButton_Accept
            // 
            this.radButton_Accept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Accept.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_checkmark_16px;
            this.radButton_Accept.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Accept.Location = new System.Drawing.Point(396, 87);
            this.radButton_Accept.Name = "radButton_Accept";
            this.radButton_Accept.Size = new System.Drawing.Size(100, 24);
            this.radButton_Accept.TabIndex = 0;
            this.radButton_Accept.Text = "Speichern";
            this.radButton_Accept.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Accept.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ProxyConfigEditor
            // 
            this.AcceptButton = this.radButton_Accept;
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(499, 114);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ProxyConfigEditor";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Updateserverinfo";
            this.Shown += new System.EventHandler(this.UpdateServerInfoEditor_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_UserProxy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Username)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Accept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_UserProxy;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Username;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Password;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton_Accept;
    }
}