﻿using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls;
using Telerik.WinControls.Themes;

namespace Pilz.Updating.Administration.GUI.My
{

    // HINWEIS: Diese Datei wird automatisch generiert und darf nicht direkt bearbeitet werden.  Wenn Sie Änderungen vornehmen möchten
    // oder in dieser Datei Buildfehler auftreten, wechseln Sie zum Projekt-Designer.
    // (Wechseln Sie dazu zu den Projekteigenschaften, oder doppelklicken Sie auf den Knoten "Mein Projekt" im
    // Projektmappen-Explorer). Nehmen Sie auf der Registerkarte "Anwendung" entsprechende Änderungen vor.
    // 
    internal partial class MyApplication
    {
        protected override bool OnStartup(StartupEventArgs eventArgs)
        {
            var success = ThemeResolutionService.LoadPackageResource("Pilz.Updating.Administration.GUI.CustomThemes.Office2019DarkPurple.tssp");
            if (success)
                ThemeResolutionService.ApplicationThemeName = "Office2019DarkPurple";
            return base.OnStartup(eventArgs);
        }
    }
}
