﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace Pilz.Updating.Administration.GUI
{
    [DesignerGenerated()]
    public partial class ApplicationVersionInput : Telerik.WinControls.UI.RadForm
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationVersionInput));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBoxControl_Version = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radDropDownList_Channel = new Telerik.WinControls.UI.RadDropDownList();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radSpinEditor_Build = new Telerik.WinControls.UI.RadSpinEditor();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Accept = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Version)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Channel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor_Build)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Accept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(50, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Version:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 33);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(39, 19);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Kanal:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 61);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(37, 19);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Build:";
            // 
            // radTextBoxControl_Version
            // 
            this.radTextBoxControl_Version.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl_Version.Location = new System.Drawing.Point(74, 3);
            this.radTextBoxControl_Version.Name = "radTextBoxControl_Version";
            this.radTextBoxControl_Version.NullText = "Bspw. 1.2.5.0";
            this.radTextBoxControl_Version.Size = new System.Drawing.Size(255, 22);
            this.radTextBoxControl_Version.TabIndex = 0;
            // 
            // radDropDownList_Channel
            // 
            this.radDropDownList_Channel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radDropDownList_Channel.DropDownAnimationEnabled = true;
            this.radDropDownList_Channel.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Channel.Location = new System.Drawing.Point(74, 31);
            this.radDropDownList_Channel.Name = "radDropDownList_Channel";
            this.radDropDownList_Channel.Size = new System.Drawing.Size(255, 22);
            this.radDropDownList_Channel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radSpinEditor_Build);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radButton_Accept);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radTextBoxControl_Version);
            this.panel1.Controls.Add(this.radDropDownList_Channel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 114);
            this.panel1.TabIndex = 3;
            // 
            // radSpinEditor_Build
            // 
            this.radSpinEditor_Build.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radSpinEditor_Build.Location = new System.Drawing.Point(74, 59);
            this.radSpinEditor_Build.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditor_Build.Name = "radSpinEditor_Build";
            this.radSpinEditor_Build.NullableValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditor_Build.Size = new System.Drawing.Size(255, 22);
            this.radSpinEditor_Build.TabIndex = 5;
            this.radSpinEditor_Build.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_delete_sign_16px;
            this.radButton_Cancel.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Cancel.Location = new System.Drawing.Point(123, 87);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(100, 24);
            this.radButton_Cancel.TabIndex = 0;
            this.radButton_Cancel.Text = "Abbrechen";
            this.radButton_Cancel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButton_Accept
            // 
            this.radButton_Accept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Accept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.radButton_Accept.Image = global::Pilz.Updating.Administration.GUI.My.Resources.Resources.icons8_checkmark_16px;
            this.radButton_Accept.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Accept.Location = new System.Drawing.Point(229, 87);
            this.radButton_Accept.Name = "radButton_Accept";
            this.radButton_Accept.Size = new System.Drawing.Size(100, 24);
            this.radButton_Accept.TabIndex = 4;
            this.radButton_Accept.Text = "Okay";
            this.radButton_Accept.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Accept.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton_Accept.Click += new System.EventHandler(this.radButton_Accept_Click);
            // 
            // ApplicationVersionInput
            // 
            this.AcceptButton = this.radButton_Accept;
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(332, 114);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ApplicationVersionInput";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Version";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl_Version)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Channel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor_Build)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Accept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl_Version;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Channel;
        private Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadButton radButton_Accept;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor_Build;
    }
}