﻿using System;
using Microsoft.VisualBasic.CompilerServices;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Administration.GUI
{
    public partial class ApplicationVersionInput
    {

        // C o n s t r u c t o r s

        public ApplicationVersionInput()
        {
            InitializeComponent();

            // Init Channel-ComboBox
            radDropDownList_Channel.Items.Add(new RadListDataItem() { Text = My.Resources.UpdatingAdministrationLangRes.Channel_Stable, Tag = Channels.Stable });
            radDropDownList_Channel.Items.Add(new RadListDataItem() { Text = My.Resources.UpdatingAdministrationLangRes.Channel_PreRelease, Tag = Channels.PreRelease });
            radDropDownList_Channel.Items.Add(new RadListDataItem() { Text = My.Resources.UpdatingAdministrationLangRes.Channel_Beta, Tag = Channels.Beta });
            radDropDownList_Channel.Items.Add(new RadListDataItem() { Text = My.Resources.UpdatingAdministrationLangRes.Channel_Alpha, Tag = Channels.Alpha });
            radDropDownList_Channel.SelectedIndex = 0;
        }

        // P r o p e r t i e s

        public Version Version
        {
            get
            {
                return new Version(radTextBoxControl_Version.Text.Trim());
            }

            set
            {
                radTextBoxControl_Version.Text = value.ToString();
            }
        }

        public Channels Channel
        {
            get
            {
                return (Channels)radDropDownList_Channel.SelectedItem.Tag;
            }

            set
            {
                foreach (var ci in radDropDownList_Channel.Items)
                {
                    if ((Channels)ci.Tag == value)
                    {
                        radDropDownList_Channel.SelectedItem = ci;
                    }
                }
            }
        }

        public int Build
        {
            get
            {
                return (int)Math.Round(radSpinEditor_Build.Value);
            }

            set
            {
                radSpinEditor_Build.Value = value;
            }
        }

        private void radButton_Accept_Click(object sender, EventArgs e)
        {

        }
    }
}