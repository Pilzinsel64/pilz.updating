﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Administration.GUI
{
    public partial class ProxyConfigEditor : RadForm
    {
        private readonly ProxyConfiguration config = null;

        public ProxyConfigEditor(ProxyConfiguration config)
        {
            this.config = config;
            InitializeComponent();
        }

        private void UpdateServerInfoEditor_Shown(object sender, EventArgs e)
        {
            radToggleSwitch_UserProxy.Value = config.UseProxyAuth;
            radTextBoxControl_Username.Text = config.Username;
            radTextBoxControl_Password.Text = config.Password ?? string.Empty;
        }

        private void ButtonX_OK_Click(object sender, EventArgs e)
        {
            config.UseProxyAuth = radToggleSwitch_UserProxy.Value;
            config.Username = radTextBoxControl_Username.Text.Trim();
            config.Password = radTextBoxControl_Password.Text;
        }
    }
}
