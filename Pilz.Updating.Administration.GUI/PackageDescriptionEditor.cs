﻿using System;
using Microsoft.VisualBasic.CompilerServices;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Administration.GUI
{
    public partial class PackageDescriptionEditor
    {

        // C o n s t r u c t o r s

        public PackageDescriptionEditor()
        {
            InitializeComponent();

            radDropDownList_Formatting.Items.AddRange(new RadListDataItem[]
            {
                new RadListDataItem { Text = "Nur Text",  Tag = UpdateNotesContentType.PlainText },
                new RadListDataItem { Text = "Markdown",  Tag = UpdateNotesContentType.Markdown },
                new RadListDataItem { Text = "HTML",      Tag = UpdateNotesContentType.HTML },
            });
        }

        // P r o p e r t i e s

        public string Titel
        {
            get
            {
                return radTextBoxControl_Titel.Text;
            }
            set
            {
                radTextBoxControl_Titel.Text = value;
            }
        }

        public string Description
        {
            get
            {
                return radTextBox_Description.Text;
            }
            set
            {
                radTextBox_Description.Text = value ?? String.Empty;
            }
        }

        public UpdateNotesContentType DescriptionType
        {
            get =>
                (UpdateNotesContentType)radDropDownList_Formatting.SelectedItem?.Tag;
            set
            {
                foreach (var item in radDropDownList_Formatting.Items)
                    if ((UpdateNotesContentType)item.Tag == value)
                        radDropDownList_Formatting.SelectedItem = item;
            }
        }
    }
}