﻿using System.Drawing;
using System.Windows.Forms;
using Pilz.Updating.GUIBase;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Pilz.Updating.Client.GUI
{
    internal partial class UpdatesAvailableDialog
    {
        public UpdatesAvailableDialog(Image appIcon, string curVersion, string curChannel, string curBuild, string newVersion, string newChannel, string newBuild, UpdateNotes updateNotes, bool installAsAdmin)
        {
            InitializeComponent();

            if (installAsAdmin)
                radButton_Install.Image = BaseFeatures.GetUacShieldImage();

            radPictureBox1.Image = appIcon ?? Icon.ToBitmap();
            radLabel_CurrentVersion.Text = curVersion;
            radLabel_CurrentVersionChannel.Text = curChannel;
            radLabel_CurrentVersionBuild.Text = curBuild;
            radLabel_AvailableVersion.Text = newVersion;
            radLabel_AvailableVersionChannel.Text = newChannel;
            radLabel_AvailableVersionBuild.Text = newBuild;

            // Update Notes
            Control updateNotesControl = null;
            switch (updateNotes.ContentType)
            {
                case UpdateNotesContentType.PlainText:
                    var newUpdateNotesControl = new RadRichTextEditor
                    {
                        Text = updateNotes.Content
                    };
                    updateNotesControl = newUpdateNotesControl;
                    break;
                case UpdateNotesContentType.Markdown:
                    updateNotesControl = new Westermo.HtmlRenderer.WinForms.HtmlPanel
                    {
                        Text = Markdig.Markdown.ToHtml(updateNotes.Content)
                    };
                    break;
                case UpdateNotesContentType.HTML:
                    updateNotesControl = new Westermo.HtmlRenderer.WinForms.HtmlPanel
                    {
                        Text = updateNotes.Content
                    };
                    break;
            }
            if (updateNotesControl is not null)
            {
                updateNotesControl.Dock = DockStyle.Fill;
                panel_ChangelogPanel.Controls.Add(updateNotesControl);
            }
        }
    }
}