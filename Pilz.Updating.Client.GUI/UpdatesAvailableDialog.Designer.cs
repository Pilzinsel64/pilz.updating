﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace Pilz.Updating.Client.GUI
{
    [DesignerGenerated()]
    internal partial class UpdatesAvailableDialog : Telerik.WinControls.UI.RadForm
    {

        // Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Wird vom Windows Form-Designer benötigt.
        private System.ComponentModel.IContainer components = new System.ComponentModel.Container();

        // Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
        // Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
        // Das Bearbeiten mit dem Code-Editor ist nicht möglich.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdatesAvailableDialog));
            panel1 = new Panel();
            radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            radButton_Install = new Telerik.WinControls.UI.RadButton();
            panel_ChangelogPanel = new Panel();
            radLabel10 = new Telerik.WinControls.UI.RadLabel();
            radLabel_CurrentVersionBuild = new Telerik.WinControls.UI.RadLabel();
            radLabel7 = new Telerik.WinControls.UI.RadLabel();
            radLabel_AvailableVersionBuild = new Telerik.WinControls.UI.RadLabel();
            radLabel9 = new Telerik.WinControls.UI.RadLabel();
            radLabel_CurrentVersionChannel = new Telerik.WinControls.UI.RadLabel();
            radLabel3 = new Telerik.WinControls.UI.RadLabel();
            radLabel_AvailableVersionChannel = new Telerik.WinControls.UI.RadLabel();
            radLabel4 = new Telerik.WinControls.UI.RadLabel();
            radLabel_CurrentVersion = new Telerik.WinControls.UI.RadLabel();
            radLabel5 = new Telerik.WinControls.UI.RadLabel();
            radLabel_AvailableVersion = new Telerik.WinControls.UI.RadLabel();
            radLabel2 = new Telerik.WinControls.UI.RadLabel();
            radLabel1 = new Telerik.WinControls.UI.RadLabel();
            radPictureBox1 = new Telerik.WinControls.UI.RadPictureBox();
            layoutControlLabelItem1 = new Telerik.WinControls.UI.LayoutControlLabelItem();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)radButton_Cancel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radButton_Install).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersionBuild).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersionBuild).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersionChannel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersionChannel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersion).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersion).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radPictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.BackColor = Color.Transparent;
            panel1.Controls.Add(radButton_Cancel);
            panel1.Controls.Add(radButton_Install);
            panel1.Controls.Add(panel_ChangelogPanel);
            panel1.Controls.Add(radLabel10);
            panel1.Controls.Add(radLabel_CurrentVersionBuild);
            panel1.Controls.Add(radLabel7);
            panel1.Controls.Add(radLabel_AvailableVersionBuild);
            panel1.Controls.Add(radLabel9);
            panel1.Controls.Add(radLabel_CurrentVersionChannel);
            panel1.Controls.Add(radLabel3);
            panel1.Controls.Add(radLabel_AvailableVersionChannel);
            panel1.Controls.Add(radLabel4);
            panel1.Controls.Add(radLabel_CurrentVersion);
            panel1.Controls.Add(radLabel5);
            panel1.Controls.Add(radLabel_AvailableVersion);
            panel1.Controls.Add(radLabel2);
            panel1.Controls.Add(radLabel1);
            panel1.Controls.Add(radPictureBox1);
            resources.ApplyResources(panel1, "panel1");
            panel1.Name = "panel1";
            // 
            // radButton_Cancel
            // 
            resources.ApplyResources(radButton_Cancel, "radButton_Cancel");
            radButton_Cancel.DialogResult = DialogResult.Cancel;
            radButton_Cancel.Image = MyIcons.icons8_delete_16px;
            radButton_Cancel.Name = "radButton_Cancel";
            // 
            // radButton_Install
            // 
            resources.ApplyResources(radButton_Install, "radButton_Install");
            radButton_Install.DialogResult = DialogResult.OK;
            radButton_Install.Image = MyIcons.icons8_software_installer_16px;
            radButton_Install.Name = "radButton_Install";
            // 
            // panel_ChangelogPanel
            // 
            resources.ApplyResources(panel_ChangelogPanel, "panel_ChangelogPanel");
            panel_ChangelogPanel.Name = "panel_ChangelogPanel";
            // 
            // radLabel10
            // 
            resources.ApplyResources(radLabel10, "radLabel10");
            radLabel10.Name = "radLabel10";
            // 
            // radLabel_CurrentVersionBuild
            // 
            resources.ApplyResources(radLabel_CurrentVersionBuild, "radLabel_CurrentVersionBuild");
            radLabel_CurrentVersionBuild.Name = "radLabel_CurrentVersionBuild";
            // 
            // radLabel7
            // 
            resources.ApplyResources(radLabel7, "radLabel7");
            radLabel7.Name = "radLabel7";
            // 
            // radLabel_AvailableVersionBuild
            // 
            resources.ApplyResources(radLabel_AvailableVersionBuild, "radLabel_AvailableVersionBuild");
            radLabel_AvailableVersionBuild.Name = "radLabel_AvailableVersionBuild";
            // 
            // radLabel9
            // 
            resources.ApplyResources(radLabel9, "radLabel9");
            radLabel9.Name = "radLabel9";
            // 
            // radLabel_CurrentVersionChannel
            // 
            resources.ApplyResources(radLabel_CurrentVersionChannel, "radLabel_CurrentVersionChannel");
            radLabel_CurrentVersionChannel.Name = "radLabel_CurrentVersionChannel";
            // 
            // radLabel3
            // 
            resources.ApplyResources(radLabel3, "radLabel3");
            radLabel3.Name = "radLabel3";
            // 
            // radLabel_AvailableVersionChannel
            // 
            resources.ApplyResources(radLabel_AvailableVersionChannel, "radLabel_AvailableVersionChannel");
            radLabel_AvailableVersionChannel.Name = "radLabel_AvailableVersionChannel";
            // 
            // radLabel4
            // 
            resources.ApplyResources(radLabel4, "radLabel4");
            radLabel4.Name = "radLabel4";
            // 
            // radLabel_CurrentVersion
            // 
            resources.ApplyResources(radLabel_CurrentVersion, "radLabel_CurrentVersion");
            radLabel_CurrentVersion.Name = "radLabel_CurrentVersion";
            // 
            // radLabel5
            // 
            resources.ApplyResources(radLabel5, "radLabel5");
            radLabel5.Name = "radLabel5";
            // 
            // radLabel_AvailableVersion
            // 
            resources.ApplyResources(radLabel_AvailableVersion, "radLabel_AvailableVersion");
            radLabel_AvailableVersion.Name = "radLabel_AvailableVersion";
            // 
            // radLabel2
            // 
            resources.ApplyResources(radLabel2, "radLabel2");
            radLabel2.Name = "radLabel2";
            // 
            // radLabel1
            // 
            resources.ApplyResources(radLabel1, "radLabel1");
            radLabel1.Name = "radLabel1";
            // 
            // radPictureBox1
            // 
            resources.ApplyResources(radPictureBox1, "radPictureBox1");
            radPictureBox1.Name = "radPictureBox1";
            // 
            // layoutControlLabelItem1
            // 
            resources.ApplyResources(layoutControlLabelItem1, "layoutControlLabelItem1");
            layoutControlLabelItem1.Bounds = new Rectangle(0, 52, 428, 39);
            layoutControlLabelItem1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            layoutControlLabelItem1.DrawText = false;
            layoutControlLabelItem1.Name = "layoutControlLabelItem1";
            layoutControlLabelItem1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            layoutControlLabelItem1.UseCompatibleTextRendering = false;
            // 
            // UpdatesAvailableDialog
            // 
            AcceptButton = radButton_Install;
            resources.ApplyResources(this, "$this");
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = radButton_Cancel;
            Controls.Add(panel1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "UpdatesAvailableDialog";
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)radButton_Cancel).EndInit();
            ((System.ComponentModel.ISupportInitialize)radButton_Install).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel10).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersionBuild).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel7).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersionBuild).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel9).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersionChannel).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel3).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersionChannel).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel4).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_CurrentVersion).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel5).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_AvailableVersion).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel2).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel1).EndInit();
            ((System.ComponentModel.ISupportInitialize)radPictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
            ResumeLayout(false);
        }

        private Panel panel1;
        private Telerik.WinControls.UI.RadPictureBox radPictureBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_CurrentVersion;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_AvailableVersion;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.LayoutControlLabelItem layoutControlLabelItem1;
        private Telerik.WinControls.UI.RadLabel radLabel_CurrentVersionBuild;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel_AvailableVersionBuild;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel_CurrentVersionChannel;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_AvailableVersionChannel;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Panel panel_ChangelogPanel;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadButton radButton_Install;
    }
}