﻿namespace Pilz.Updating.Client.GUI
{
    public partial class SimpleActionDialog
    {
        public SimpleActionDialog()
        {
            InitializeComponent();
            SetCurrentState(UpdateStatus.Waiting);
        }

        public void SetCurrentState(UpdateStatus curAction)
        {
            string progressText = curAction switch
            {
                UpdateStatus.Waiting => UpdatingClientGuiLangRes.SimpleActions_Waiting,
                UpdateStatus.Searching => UpdatingClientGuiLangRes.SimpleActions_Searching,
                UpdateStatus.DownloadingPackage => UpdatingClientGuiLangRes.SimpleActions_DownloadingPackage,
                UpdateStatus.DownloadingInstaller => UpdatingClientGuiLangRes.SimpleActions_DownloadingInstaller,
                UpdateStatus.StartingInstaller => UpdatingClientGuiLangRes.SimpleActions_StartingInstaller,
                _ => string.Empty,
            };

            radProgressBar1.Visible = false;
            radWaitingBar1.Text = progressText;
            radWaitingBar1.BringToFront();
            radWaitingBar1.StartWaiting();
        }
    }
}