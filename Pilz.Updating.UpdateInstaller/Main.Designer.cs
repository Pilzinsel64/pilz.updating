﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace Pilz.Updating.UpdateInstaller
{
    [DesignerGenerated()]
    public partial class Main : Telerik.WinControls.UI.RadForm
    {

        // Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Wird vom Windows Form-Designer benötigt.
        private System.ComponentModel.IContainer components;

        // Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
        // Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
        // Das Bearbeiten mit dem Code-Editor ist nicht möglich.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            _Panel1 = new Panel();
            radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            radLabel_Status = new Telerik.WinControls.UI.RadLabel();
            radLabel_Header = new Telerik.WinControls.UI.RadLabel();
            _Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)radWaitingBar1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_Status).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_Header).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            SuspendLayout();
            // 
            // _Panel1
            // 
            _Panel1.BackColor = Color.Transparent;
            _Panel1.Controls.Add(radWaitingBar1);
            _Panel1.Controls.Add(radLabel_Status);
            _Panel1.Controls.Add(radLabel_Header);
            _Panel1.Dock = DockStyle.Fill;
            _Panel1.Location = new Point(0, 0);
            _Panel1.Name = "_Panel1";
            _Panel1.Size = new Size(692, 87);
            _Panel1.TabIndex = 1;
            // 
            // radWaitingBar1
            // 
            radWaitingBar1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            radWaitingBar1.Location = new Point(3, 66);
            radWaitingBar1.Name = "radWaitingBar1";
            radWaitingBar1.Size = new Size(130, 18);
            radWaitingBar1.TabIndex = 3;
            radWaitingBar1.Text = "radWaitingBar1";
            radWaitingBar1.WaitingIndicators.Add(dotsLineWaitingBarIndicatorElement1);
            radWaitingBar1.WaitingIndicatorSize = new Size(100, 14);
            radWaitingBar1.WaitingSpeed = 80;
            radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.RadWaitingBarElement)radWaitingBar1.GetChildAt(0)).WaitingSpeed = 80;
            ((Telerik.WinControls.UI.WaitingBarContentElement)radWaitingBar1.GetChildAt(0).GetChildAt(0)).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)radWaitingBar1.GetChildAt(0).GetChildAt(0).GetChildAt(0)).Dash = false;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // radLabel_Status
            // 
            radLabel_Status.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            radLabel_Status.AutoSize = false;
            radLabel_Status.Location = new Point(139, 66);
            radLabel_Status.Name = "radLabel_Status";
            radLabel_Status.Size = new Size(550, 18);
            radLabel_Status.TabIndex = 2;
            radLabel_Status.Text = "Idle ...";
            radLabel_Status.TextImageRelation = TextImageRelation.ImageBeforeText;
            // 
            // radLabel_Header
            // 
            radLabel_Header.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            radLabel_Header.AutoSize = false;
            radLabel_Header.Image = MyIcons.icons8_installing_updates_48px;
            radLabel_Header.Location = new Point(3, 3);
            radLabel_Header.Name = "radLabel_Header";
            radLabel_Header.Size = new Size(686, 57);
            radLabel_Header.TabIndex = 1;
            radLabel_Header.Text = "<html><span style=\"font-size: 18pt; color: #b7472a\"><b>SM64 ROM Manager wird aktuallisierung ...</b></span></html>";
            radLabel_Header.TextAlignment = ContentAlignment.MiddleCenter;
            radLabel_Header.TextImageRelation = TextImageRelation.ImageBeforeText;
            // 
            // Main
            // 
            AutoScaleBaseSize = new Size(7, 15);
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(692, 87);
            Controls.Add(_Panel1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "Main";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Installing";
            _Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)radWaitingBar1).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_Status).EndInit();
            ((System.ComponentModel.ISupportInitialize)radLabel_Header).EndInit();
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
            ResumeLayout(false);
        }

        private Panel _Panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Status;
        private Telerik.WinControls.UI.RadLabel radLabel_Header;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;

        internal Panel Panel1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Panel1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Panel1 != null)
                {
                }

                _Panel1 = value;
                if (_Panel1 != null)
                {
                }
            }
        }
    }
}