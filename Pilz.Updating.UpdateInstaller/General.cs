﻿using System.IO;
using System.Reflection;
using Pilz.Plugins;
using System.Configuration;
using Z.Reflection.Extensions;

namespace Pilz.Updating.UpdateInstaller
{
    internal static class General
    {
        private static string p = null;

        public static string MyAppPath
        {
            get
            {
                if (string.IsNullOrEmpty(p))
                    p = Path.GetDirectoryName(IO.Extensions.GetExecutablePath());
                return p;
            }
        }

        public static void LoadAddons(Lib.UpdateInstaller installer)
        {
            var pluginsPath = Path.Combine(MyAppPath, "AddOns");
            if (Directory.Exists(pluginsPath))
            {
                foreach (var subdir in Directory.GetDirectories(pluginsPath, string.Empty, SearchOption.TopDirectoryOnly))
                {
                    var pluginPath = Path.Combine(subdir, Path.GetFileName(subdir) + ".dll");
                    if (File.Exists(pluginPath))
                        PluginManager.Instance.LoadPlugin(pluginPath, installer);
                }
            }
        }
    }
}