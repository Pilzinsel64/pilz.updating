﻿using Newtonsoft.Json;
using Pilz.Cryptography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.Administration
{
    public class UpdateServerConfig
    {
        public bool UseProxyForWebDAV { get; set; } = false;
        public string ServerAdress { get; set; }
        public string PublicPackageBaseURL { get; set; }
        public string UpdateInfoFilename { get; set; }
        public string Username { get; set; }

        [JsonProperty("PasswordV3")]
        public SecureString Password { get; set; }
    }
}
