﻿using Newtonsoft.Json;
using Pilz.Cryptography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.Administration
{
    public class ProxyConfiguration
    {
        public bool UseProxyAuth { get; set; }
        public string Username { get; set; }
        [JsonProperty("PasswordV3")]
        public SecureString Password { get; set; }
    }
}
