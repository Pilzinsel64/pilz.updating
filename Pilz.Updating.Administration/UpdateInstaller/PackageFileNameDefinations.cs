﻿namespace Pilz.Updating.UpdateInstaller
{
    public static class PackageFileNameDefinations
    {
        public const string ZIP_PACKAGE_FILENAME = "updatepackage.zip";
        public const string ZIP_UPDATE_INSTALLER_ADDONS_DIRECTORY = "installer_addons";
        public const string ZIP_APP_DATA_FILES_DIRECTORY = "appdata";
    }
}