﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating.Administration.Discord
{
    public class DiscordBotConfig
    {
        public string DiscordBotToken { get; set; } = string.Empty;
        public string DefaultAppName { get; set; } = string.Empty;
        public string UpdateNotificationRoll { get; set; } = string.Empty;
        public bool UseProxy { get; set; } = true;
        public Dictionary<Channels, string> DefaultUpdateMessages { get; } = new()
        {
            { Channels.Stable, null },
            { Channels.PreRelease, null },
            { Channels.Beta, null },
            { Channels.Alpha, null }
        };

        [JsonProperty, Obsolete]
        private string DefaultUpdateMessage
        {
            set
            {
                foreach (var keys in DefaultUpdateMessages.Keys)
                    DefaultUpdateMessages[keys] = value;
            }
        }
    }
}
