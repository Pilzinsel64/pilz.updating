﻿using System.Collections.Generic;

namespace Pilz.Updating.Administration.Packaging
{
    public class UpdatePackageTemplate
    {
        public string FilesToCopyPath { get; set; }
        public List<string> UpdateInstallerAddOns { get; set; } = new List<string>();
    }
}