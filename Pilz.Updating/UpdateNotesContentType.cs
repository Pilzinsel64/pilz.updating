﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating
{
    public enum UpdateNotesContentType
    {
        PlainText,
        Markdown,
        HTML
    }
}
