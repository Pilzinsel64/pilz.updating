﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Updating
{
    public class UpdateNotes
    {
        public string Content { get; set; }
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public UpdateNotesContentType ContentType { get; set; } = UpdateNotesContentType.PlainText;
    }
}
