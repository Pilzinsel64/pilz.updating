﻿
namespace Pilz.Updating
{
    public enum Channels
    {
        Stable,
        PreRelease,
        Beta,
        Alpha
    }
}